import CRUD.RestaurantCRUD;
import com.mongodb.client.MongoDatabase;
import connection.ConnectionMongoCluster;
import models.RestaurantDTO;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.util.List;

import static java.util.Arrays.asList;

public class Main {

    private static final RestaurantCRUD restaurantCRUD = new RestaurantCRUD();

    public static void main(String[] args) {
        // Database
        MongoDatabase db = ConnectionMongoCluster.getDatabase("ITB");

        // ==== PR 1 ====
        // EX 1
//        dropCollection(db, "restaurants");
        // EX 2
//        loadCollectionFromFile(db, "restaurants", "src/main/resources/restaurants.json");
        // EX 3
//        insertRestaurant(db, new Document()
//                .append("_id", new ObjectId())
//                .append("address",
//                        new Document("building", "3850")
//                                .append("street", "Richmond Avenue")
//                                .append("zipcode", "10312")
//                )
//                .append("borough", "Staten Island")
//                .append("cuisine", "Pizza")
//                .append("name", "David Vaquer - El pizzero espagnol")
//                .append("restaurant_id", "99999999")
//                .append("grades", List.of()) // Aquest últim "append" no ho demanes però és necessari per l'exercici 13.
//        );
//        insertRestaurant(db, new Document()
//                .append("_id", new ObjectId())
//                .append("address",
//                        new Document("building", "24")
//                                .append("coord", asList(-73.9812198, 40.7509706))
//                                .append("street", "East 39 Street")
//                                .append("zipcode", "10016")
//                )
//                .append("borough", "Manhattan")
//                .append("cuisine", "American")
//                .append("name", "Joan_Gomez's Hamburgers")
//                .append("restaurant_id", "999999991")
//                .append("grades", List.of()) // Aquest últim "append" no ho demanes però és necessari per l'exercici 13.
//        );
        // EX 4
//        createIndex(db, "restaurants", "restaurant_id");
//        showIndexes(db, "restaurants");
        // EX 5
//        restaurantsByBoroughAndCuisine(db, "restaurants", "Manhattan", "Seafood");
        // EX 6
//        restaurantsByZipCode(db, "restaurants", "10016");
        // EX 7
//        restaurantsByBoroughExcludingCuisine(db, "restaurants", "Queens", "American");
        // EX 8
//        restaurantsByBuilding(db, "restaurants", "3406");
        // EX 9
//        changeRestaurantZipcodeByStreet(db, "restaurants", "Charles Street", "30033");
        // EX 10
//        rateRestaurantByCuisine(db, "restaurants", "Caribbean", "*****");
         // EX 11 - No hi ha un restaurant amb el "restaurant_id" especificat a l'enunciat he utilitzat un d'existent.
//        changeRestaurantCoords(db, "restaurants", "999999991", -73.996970, 40.72532);
        // EX 12
//        removeRestaurantsByCuisine(db, "restaurants", "Delicatessen");
        // EX 13
//        countRestaurantsGradesAndSumScore(db, "restaurants", "grades");

        // ==== PR 2 ====
        // EX 1
//        countRestaurantsByBoroughDescending(db, "restaurants");
        // EX 2
//        boroughWithLessRestaurants(db, "restaurants");
        // EX 3
//        restaurantsByCuisineDescending(db, "restaurants");
        // EX 4
//        restaurantGte7Grades(db, "restaurants");
        // EX 5
//        restaurantCuisineByBorough(db, "restaurants");

        ConnectionMongoCluster.closeConnection();
    }

    /**
     * Method to get all types of "cuisine" per "borough".
     * @param db MongoDatabase
     * @param collection String
     */
    private static void restaurantCuisineByBorough(MongoDatabase db, String collection) {
        restaurantCRUD.restaurantCuisineByBorough(db, collection);
    }

    /**
     * Method to get all restaurants with 7 or more "grades".
     * @param db MongoDatabase
     * @param collection String
     */
    private static void restaurantGte7Grades(MongoDatabase db, String collection) {
        restaurantCRUD.restaurantGte7Grades(db, collection);
    }

    /**
     * Method to get the amount of restaurants per type of "cuisine".
     * @param db MongoDatabase
     * @param collection String
     */
    private static void restaurantsByCuisineDescending(MongoDatabase db, String collection) {
        restaurantCRUD.restaurantsByCuisineDescending(db, collection);
    }

    /**
     * Method to know the borough with less restaurants.
     * @param db MongoDatabase
     * @param collection String
     */
    private static void boroughWithLessRestaurants(MongoDatabase db, String collection) {
        restaurantCRUD.boroughWithLessRestaurants(db, collection);
    }

    /**
     * Method to get the sum of restaurants by "borough" field.
     * @param db MongoDatabase
     * @param collection String
     */
    private static void countRestaurantsByBoroughDescending(MongoDatabase db, String collection) {
        restaurantCRUD.countByBoroughDescending(db, collection);
    }

    /**
     * Method to get the count of "grades" and the sum of its "score" per restaurant.
     * @param db MongoDatabase
     * @param collection String
     * @param grades String
     */
    private static void countRestaurantsGradesAndSumScore(MongoDatabase db, String collection, String grades) {
        restaurantCRUD.countGradesAndSumScore(db, collection, grades);
    }

    /**
     * Method to remove restaurants by a certain "cuisine" field.
     * @param db MongoDatabase
     * @param collection String
     * @param field String
     */
    private static void removeRestaurantsByCuisine(MongoDatabase db, String collection, String field) {
        restaurantCRUD.removeByCuisine(db, collection, field);
    }

    /**
     * Method to change a restaurant "coord" field.
     * @param db MongoDatabase
     * @param collection String
     * @param restaurant_id String
     * @param newLat double
     * @param newLng double
     */
    private static void changeRestaurantCoords(MongoDatabase db, String collection, String restaurant_id, double newLat, double newLng) {
        restaurantCRUD.updateCoords(db, collection, restaurant_id, newLat, newLng);
    }

    /**
     * Method to add a rating field to a restaurant by its "cuisine" field.
     * @param db MongoDatabase
     * @param collection String
     * @param cuisine String
     * @param rating String
     */
    private static void rateRestaurantByCuisine(MongoDatabase db, String collection, String cuisine, String rating) {
        restaurantCRUD.rateByCuisine(db, collection, cuisine, rating);
    }

    /**
     * Method to modify a restaurants "zipcode" by its "street" field.
     * @param db MongoDatabase
     * @param collection String
     * @param street String
     * @param newZipcode String
     */
    private static void changeRestaurantZipcodeByStreet(MongoDatabase db, String collection, String street, String newZipcode) {
        restaurantCRUD.modifyZipcodeByStreet(db, collection, street, newZipcode );
    }

    /**
     * Method to find restaurants by its building.
     * @param db MongoDatabase
     * @param collection String
     * @param building String
     */
    private static void restaurantsByBuilding(MongoDatabase db, String collection, String building) {
        restaurantCRUD.findByBuilding(db, collection, building);
    }

    /**
     * Method to find restaurant by "borough" excluding "cuisine".
     * @param db MongoDatabase
     * @param collection String
     * @param borough String
     * @param cuisine String
     */
    private static void restaurantsByBoroughExcludingCuisine(MongoDatabase db, String collection, String borough, String cuisine) {
        restaurantCRUD.getByBoroughExcludingCuisine(db, collection, borough, cuisine);
    }

    /**
     * Method to find restaurants by "zipcode" field.
     * @param db MongoDatabase
     * @param collection String
     * @param zipcode String
     */
    private static void restaurantsByZipCode(MongoDatabase db, String collection, String zipcode) {
        restaurantCRUD.restaurantsByZipCode(db, collection, zipcode);
    }

    /**
     * Method to find a restaurant by "borough" and "cuisine" fields.
     * @param db MongoDatabase
     * @param collection String
     * @param borough String
     * @param cuisine String
     */
    private static void restaurantsByBoroughAndCuisine(MongoDatabase db, String collection, String borough, String cuisine) {
        restaurantCRUD.getByBoroughAndCuisine(db, collection, borough, cuisine);
    }

    /**
     * Method to show all indexes from a collection.
     * @param db MongoDatabase
     * @param collection String
     */
    private static void showIndexes(MongoDatabase db, String collection) {
        restaurantCRUD.showIndexes(db, collection);
    }

    /**
     * Method to create an index in a collection.
     * @param db MongoDatabase
     * @param collection String
     * @param key String
     */
    private static void createIndex(MongoDatabase db, String collection, String key) {
        restaurantCRUD.createIndex(db, collection, key);
    }

    /**
     * Method to add a register to "restaurants" collection.
     * @param db MongoDatabase
     * @param document Document
     */
    private static void insertRestaurant(MongoDatabase db, Document document) {
        restaurantCRUD.insertRestaurant(db, document);
    }

    /**
     * Method to create a collection from a JSON file.
     * @param db MongoDatabase
     * @param collection String
     * @param route String
     */
    private static void loadCollectionFromFile(MongoDatabase db, String collection, String route) {
        // Drop collection
        dropCollection(db, collection);
        // Get file array
        RestaurantDTO[] restaurants = restaurantCRUD.getRestaurantsFromFile(route);
        // Insert pojo array into collection
        restaurantCRUD.loadRestaurants(restaurants, db);
    }

    /**
     * Method to drop a desired collection and list the others left.
     * @param db MongoDatabase
     * @param collection String
     */
    private static void dropCollection(MongoDatabase db, String collection) {
        restaurantCRUD.dropCollection(db, collection);
    }

}