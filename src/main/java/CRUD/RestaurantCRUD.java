package CRUD;

import com.google.gson.Gson;
import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import com.mongodb.MongoException;
import com.mongodb.client.*;
import com.mongodb.client.model.Indexes;
import com.mongodb.client.model.Projections;
import com.mongodb.client.model.Sorts;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.InsertOneResult;
import com.mongodb.client.result.UpdateResult;
import models.RestaurantDTO;
import org.bson.Document;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.bson.conversions.Bson;

import javax.print.Doc;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.mongodb.client.model.Accumulators.*;
import static com.mongodb.client.model.Aggregates.*;
import static com.mongodb.client.model.Filters.*;
import static com.mongodb.client.model.Sorts.ascending;
import static com.mongodb.client.model.Updates.set;
import static java.util.Arrays.asList;
import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

public class RestaurantCRUD {

    public void dropCollection(MongoDatabase db, String collection) {

        System.out.println("List of existing collections:\n");

        // Drop desired collection
        MongoIterable<String> list = db.listCollectionNames();
        list.forEach(col -> {
            if (col.equals(collection)) {
                db.getCollection(collection).drop();
                System.out.println("Deleted \"" + collection + "\" collection.\n");
            }
        });

        // List
        list = db.listCollectionNames();
        list.forEach(col ->
                System.out.println("\t· " + col)
        );

    }

    public RestaurantDTO[] getRestaurantsFromFile(String route) {
        
        StringBuilder stringFile = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new FileReader(route))) {
            String linea;
            while ((linea = br.readLine()) != null) {
                stringFile.append(linea);
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }

        Gson gson = new Gson();
        return gson.fromJson(stringFile.toString(), RestaurantDTO[].class);

    }

    public void loadRestaurants(RestaurantDTO[] restaurants, MongoDatabase db) {

        CodecRegistry pojoCodecRegistry = fromRegistries(MongoClient.getDefaultCodecRegistry(),
                fromProviders(PojoCodecProvider.builder().automatic(true).build()));

        db = db.withCodecRegistry(pojoCodecRegistry);

        // Collection
        MongoCollection<RestaurantDTO> col = db.getCollection("restaurants", RestaurantDTO.class);

        for (RestaurantDTO r : restaurants) {
            System.out.println("Adding... " + r + "\n");
            col.insertOne(r);
        }
        System.out.println("Collection inserted successfully!\n");

    }

    public void insertRestaurant(MongoDatabase db, Document document) {

        MongoCollection<Document> col = db.getCollection("restaurants");
        try {
            InsertOneResult result = col.insertOne(document);
            System.out.println("Successfully inserted document: " + document.toJson() + "\n");

        } catch (MongoException e) {
            e.printStackTrace();
        }

    }

    public void createIndex(MongoDatabase db, String collection, String key) {

        db.getCollection(collection).dropIndexes();
        db.getCollection(collection).createIndex(Indexes.ascending(key));

    }

    public void showIndexes(MongoDatabase db, String collection) {

        MongoCollection<Document> col = db.getCollection(collection);
        for ( Document index : col.listIndexes() ) {
            System.out.println(index.toJson());
        }

    }

    public void getByBoroughAndCuisine(MongoDatabase db, String collection, String borough, String cuisine) {

        db.getCollection(collection).find(
                and(
                        eq("borough", borough),
                        eq("cuisine", cuisine)
                )
        ).projection(new Document("_id", 0)
                .append("name", 1)
                .append("borough", 1)
                .append("cuisine", 1)
        ).forEach(System.out::println);

    }

    public void restaurantsByZipCode(MongoDatabase db, String collection, String zipcode) {

        db.getCollection(collection).find(
                eq("address.zipcode", zipcode)
        ).projection(new Document("_id", 0)
                .append("name", 1)
//                .append("address.zipcode", 1)
        ).forEach(System.out::println);

    }

    public void getByBoroughExcludingCuisine(MongoDatabase db, String collection, String borough, String cuisine) {

        db.getCollection(collection).find(
                and(
                        eq("borough", borough),
                        ne("cuisine", cuisine)
                )
        ).projection(new Document("_id", 0)
                .append("name", 1)
                .append("borough", 1)
                .append("cuisine", 1)
        ).forEach(System.out::println);

    }

    public void findByBuilding(MongoDatabase db, String collection, String building) {

        db.getCollection(collection).find(
                eq("address.building", building)
        ).projection(new Document("_id", 0)
                .append("name", 1)
                .append("cuisine", 1)
//                .append("address.building", 1)
        ).forEach(System.out::println);

    }

    public void modifyZipcodeByStreet(MongoDatabase db, String collection, String street, String newZipcode) {

        MongoCollection<Document> col = db.getCollection(collection);

        UpdateResult updateResult = col.updateMany(
                eq("address.street", street),
                set("address.zipcode", newZipcode)
        );

        System.out.println("Rows affected: " + updateResult.getModifiedCount());

    }

    public void rateByCuisine(MongoDatabase db, String collection, String cuisine, String rating) {

        MongoCollection<Document> col = db.getCollection(collection);

        UpdateResult updateResult = col.updateMany(
                eq("cuisine", cuisine),
                set("stars", rating)
        );

        System.out.println("Rows affected " + updateResult.getModifiedCount());

    }

    public void updateCoords(MongoDatabase db, String collection, String restaurant_id, double newLat, double newLng) {

        MongoCollection<Document> col = db.getCollection(collection);

        // Actual info
        System.out.println("\nOld info:");
        col.find(
                eq("restaurant_id", restaurant_id)
        ).projection(new Document("_id", 0)
                .append("name", 1)
                .append("restaurant_id", 1)
                .append("address.coord", 1)
        ).forEach(System.out::println);

        // Updating info
        UpdateResult updateResult = col.updateMany(
                eq("restaurant_id", restaurant_id),
                set("address.coord", List.of(newLat, newLng))
        );
        System.out.println("Rows affected " + updateResult.getModifiedCount());

        // Updated info
        System.out.println("\nUpdated info:");
        col.find(
                eq("restaurant_id", restaurant_id)
        ).projection(new Document("_id", 0)
                .append("name", 1)
                .append("restaurant_id", 1)
                .append("address.coord", 1)
        ).forEach(System.out::println);

    }

    public void removeByCuisine(MongoDatabase db, String collection, String field) {

        MongoCollection<Document> col = db.getCollection(collection);

        DeleteResult deleteResult = col.deleteMany(
                eq("cuisine", field)
        );

        System.out.println("Rows affected " + deleteResult.getDeletedCount());

    }

    public void countGradesAndSumScore(MongoDatabase db, String collection, String grades) {

        MongoCollection<Document> col = db.getCollection(collection);

        Bson gradesCol = project(
                Projections.fields(Projections.excludeId(),
                        Projections.include("name"),
                        Projections.include(grades),
                        Projections.computed(
                                "numGrades",
                                Projections.computed("$size", "$grades")
                        ),
                        Projections.computed(
                                "sumScore",
                                Projections.computed("$sum", "$grades.score")
                        )
                )
        );

        col.aggregate(List.of(gradesCol))
                .forEach(r -> {
                    System.out.println("Restaurant: " + r.get("name"));
                    System.out.println("Number of grades: " + r.get("numGrades"));
                    System.out.println("Score sum: " + r.get("sumScore") + "\n");
                });

    }

    public void countByBoroughDescending(MongoDatabase db, String collection) {

        AggregateIterable<Document> doc = db.getCollection(collection)
                .aggregate(
                        asList(
                                group("$borough", sum("RestaurantsTotal", 1)),
                                sort(Sorts.descending("RestaurantsTotal"))
                        )
                );

        for ( Document d : doc ) {
            System.out.println(d.get("_id") + ": " + d.get("RestaurantsTotal"));
        }

    }

    public void boroughWithLessRestaurants(MongoDatabase db, String collection) {

        Document doc = db.getCollection(collection)
                .aggregate(
                        asList(
                                group("$borough", sum("RestaurantsTotal", 1)),
                                sort(Sorts.ascending("RestaurantsTotal"))
                        )
                ).first();

        assert doc != null;
        System.out.println(doc.get("_id") + ": " + doc.get("RestaurantsTotal"));

    }

    public void restaurantsByCuisineDescending(MongoDatabase db, String collection) {

        AggregateIterable<Document> doc = db.getCollection(collection)
                .aggregate(
                        asList(
                                group("$cuisine", sum("RestaurantsTotal", 1)),
                                sort(Sorts.descending("RestaurantsTotal"))
                        )
                );

        for ( Document d : doc ) {
            System.out.println(d.get("_id") + ": " + d.get("RestaurantsTotal"));
        }

    }

    public void restaurantGte7Grades(MongoDatabase db, String collection) {

        MongoCollection<Document> col = db.getCollection(collection);

        Bson gradesCol = project(
                Projections.fields(Projections.excludeId(),
                        Projections.include("name"),
                        Projections.computed(
                                "numGrades",
                                Projections.computed("$size", "$grades"))
                )
        );

        col.aggregate(asList(gradesCol, sort(Sorts.descending("numGrades"))))
                .forEach( res -> {
                    if (res.getInteger("numGrades") >= 7) {
                        System.out.println("Name: " + res.get("name")+"\nGrades: " + res.get("numGrades") + "\n");
                    }
                });


    }

    public void restaurantCuisineByBorough(MongoDatabase db, String collection) {

        MongoCollection<Document> col = db.getCollection(collection);

        List<String> boroughs = new ArrayList<>();

        col.aggregate(
                List.of(
                        group("$borough")
                )
        ).forEach(res -> boroughs.add((String) res.get("_id")));

        for (String b : boroughs) {
            System.out.println(b + ":");
            DistinctIterable<String> dis = col.distinct("cuisine", eq("borough", b), String.class);
            dis.forEach(c -> System.out.println("\t- " + c));
            System.out.println();
        }

    }
}
