package models;

import java.io.Serializable;

public class DateDTO implements Serializable {
	private long date;

	public void setDate(long date){
		this.date = date;
	}

	public long getDate(){
		return date;
	}

	@Override
 	public String toString(){
		return 
			"DateDTO{" + 
			"$date = '" + date + '\'' + 
			"}";
		}
}