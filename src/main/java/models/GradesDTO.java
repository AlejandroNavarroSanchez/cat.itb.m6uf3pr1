package models;

import java.io.Serializable;

public class GradesDTO implements Serializable {
	private DateDTO date;
	private String grade;
	private int score;

	public void setDate(DateDTO date){
		this.date = date;
	}

	public DateDTO getDate(){
		return date;
	}

	public void setGrade(String grade){
		this.grade = grade;
	}

	public String getGrade(){
		return grade;
	}

	public void setScore(int score){
		this.score = score;
	}

	public int getScore(){
		return score;
	}

	@Override
 	public String toString(){
		return 
			"GradesDTO{" + 
			"date = '" + date + '\'' + 
			",grade = '" + grade + '\'' + 
			",score = '" + score + '\'' + 
			"}";
		}
}