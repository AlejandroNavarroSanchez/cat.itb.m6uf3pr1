package models;

import java.util.List;
import java.io.Serializable;

public class RestaurantDTO implements Serializable {
	private AddressDTO address;
	private String borough;
	private String cuisine;
	private List<GradesDTO> grades;
	private String name;
	private String restaurantId;

	public void setAddress(AddressDTO address){
		this.address = address;
	}

	public AddressDTO getAddress(){
		return address;
	}

	public void setBorough(String borough){
		this.borough = borough;
	}

	public String getBorough(){
		return borough;
	}

	public void setCuisine(String cuisine){
		this.cuisine = cuisine;
	}

	public String getCuisine(){
		return cuisine;
	}

	public void setGrades(List<GradesDTO> grades){
		this.grades = grades;
	}

	public List<GradesDTO> getGrades(){
		return grades;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setRestaurantId(String restaurantId){
		this.restaurantId = restaurantId;
	}

	public String getRestaurantId(){
		return restaurantId;
	}

	@Override
 	public String toString(){
		return 
			"RestaurantDTO{" + 
			"address = '" + address + '\'' + 
			",borough = '" + borough + '\'' + 
			",cuisine = '" + cuisine + '\'' + 
			",grades = '" + grades + '\'' + 
			",name = '" + name + '\'' + 
			",restaurant_id = '" + restaurantId + '\'' + 
			"}";
		}
}